import Vue from 'vue'
import Vuelidate from 'vuelidate'
import VuelidateErrorExtractor, { templates } from 'vuelidate-error-extractor'

const messages = {
  required: '{attribute} wajib diisi',
  email: '{attribute} bukan format yang sesuai',
  passwordSameAs: '{attribute} tidak sesuai dengan password baru',
  numeric: '{attribute} harus dalam format angka'
}

const attributes = {
  bankName: 'Nama Bank',
  senderName: 'Nama Pengirim',
  amount: 'Jumlah',
  email: 'Email',
  password: 'Kata Sandi',
  currentPassword: 'Password sekarang',
  newPassword: 'Password baru',
  confirmationPassword: 'Konfirmasi password',
  name: 'Nama',
  phoneNumber: 'Nomor telepon',
  title: 'Judul',
  description: 'Deskripsi'
}

Vue.use(Vuelidate)
Vue.use(VuelidateErrorExtractor, {
  messages,
  attributes
})

Vue.component('AppForm', templates.FormWrapper)
