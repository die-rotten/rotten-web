import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import middleware from '../middlewares/middleware'
import { auth } from '../middlewares/auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: false,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('@/views/Register.vue')
  },
  {
    path: '/packages',
    name: 'packages.index',
    component: () => import('@/views/Customer/Package/Package.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: false,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/packages/:id',
    component: () => import('@/views/Customer/Package/PackageDetail.vue'),
    children: [
      {
        path: '',
        name: 'packages.details',
        component: () => import('@/components/modules/Package/PackageDetail.vue'),
        beforeEnter: middleware([
          auth
        ]),
        meta: {
          requiresAuth: false,
          roleIds: [
            2
          ]
        }
      },
      {
        path: 'books/:bookId',
        name: 'books.details',
        component: () => import('@/components/modules/Book/BookDetail.vue'),
        beforeEnter: middleware([
          auth
        ]),
        meta: {
          requiresAuth: false,
          roleIds: [
            2
          ]
        }
      }
    ]
  },
  {
    path: '/my/packages',
    name: 'myPackages.index',
    component: () => import('@/views/Customer/MyPackage/MyPackage.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: true,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/my/packages/:id',
    name: 'myPackages.detail',
    component: () => import('@/views/Customer/MyPackage/MyPackageDetail.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: true,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/my/packages/:id/books/:bookId/read/:contentId',
    name: 'myBooks.read',
    component: () => import('@/views/Customer/MyBook/MyBookDetail.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: true,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/my/profile',
    name: 'myProfile.detail',
    component: () => import('@/views/Customer/MyProfile/MyProfile.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: true,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/my/setting',
    name: 'mySetting.detail',
    component: () => import('@/views/Customer/MyProfile/MyProfileSetting.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: true,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/my/transactions/:id/confirm',
    name: 'confirmationTransaction.index',
    component: () => import('@/views/Customer/ConfirmPayment.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: true,
      roleIds: [
        2
      ]
    }
  },
  {
    path: '/authors/profile',
    name: 'authors.profile',
    component: () => import('@/views/Author/MyProfile.vue'),
    beforeEnter: middleware([
      auth
    ]),
    meta: {
      requiresAuth: true,
      roleIds: [
        4
      ]
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
