import bookPackage from '@/services/bookPackage'
import myPackage from '@/services/myPackage'

const state = {
  myPackages: [],
  myPackage: {},

  isFetching: false,
  isFetchingError: null,

  isFinding: false,
  isFindingError: null
}

const mutations = {
  setFetching (state) {
    state.isFetching = true
    state.isFetchingError = null
  },

  setFetchingSuccess (state, data) {
    state.myPackages = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setFinding (state) {
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    state.myPackage = data
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  }
}

const actions = {
  async get ({ commit }) {
    try {
      commit('setFetching')
      const res = await bookPackage.getMyPackage()
      commit('setFetchingSuccess', res)
    } catch (error) {
      commit('setFetchingError', error)
    }
  },

  async find ({ commit }, id) {
    try {
      commit('setFinding')
      const res = await myPackage.findMyPackage(id)
      commit('setFindingSuccess', res)
    } catch (error) {
      commit('setFindingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
