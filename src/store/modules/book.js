import book from '@/services/book'

const state = {
  book: {},

  isFinding: false,
  isFindingError: null
}

const mutations = {
  setFinding (state) {
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    state.book = data
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  }
}

const actions = {
  async find ({ commit }, id) {
    try {
      commit('setFinding')
      const res = await book.find(id)
      commit('setFindingSuccess', res)
    } catch (error) {
      commit('setFindingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
