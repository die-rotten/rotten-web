import myTransaction from '@/services/myTransaction'

const state = {
  transactions: [],
  transaction: {},

  isFetching: false,
  isFetchingError: null,

  isFinding: false,
  isFindingError: null,

  isCreating: false,
  isCreatingError: null,

  isConfirmPayment: false,
  isConfirmPaymentError: null
}

const mutations = {
  setFetching (state) {
    state.isFetching = true
    state.isFetchingError = null
  },

  setFetchingSuccess (state, data) {
    state.transactions = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setFinding (state) {
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    state.transaction = data
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  },

  setCreating (state) {
    state.isCreating = true
    state.isCreatingError = null
  },

  setCreatingSuccess (state) {
    state.isCreating = false
  },

  setCreatingError (state, error) {
    state.isCreatingError = error
    state.isCreating = false
  },

  setConfirmPayment (state) {
    state.isConfirmPayment = true
    state.isConfirmPaymentError = null
  },

  setConfirmPaymentSuccess (state) {
    state.isConfirmPayment = false
  },

  setConfirmPaymentError (state, error) {
    state.isConfirmPaymentError = error
    state.isConfirmPayment = false
  }
}

const actions = {
  async get ({ commit }, params) {
    try {
      commit('setFetching')
      const res = await myTransaction.get(params)
      commit('setFetchingSuccess', res)
    } catch (error) {
      commit('setFetchingError', error)
    }
  },

  async find ({ commit }, id) {
    try {
      commit('setFinding')
      const res = await myTransaction.find(id)
      commit('setFindingSuccess', res)
    } catch (error) {
      commit('setFindingError', error)
    }
  },

  async store ({ commit }, payload) {
    try {
      commit('setCreating')
      await myTransaction.store(payload)
      commit('setCreatingSuccess')
    } catch (error) {
      commit('setCreatingError', error)
    }
  },

  async confirmPayment ({ commit }, payload) {
    try {
      commit('setConfirmPayment')
      await myTransaction.confirmPayment(payload.id, payload)
      commit('setConfirmPaymentSuccess')
    } catch (error) {
      commit('setConfirmPaymentError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
