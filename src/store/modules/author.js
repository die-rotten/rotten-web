import author from '@/services/author'

const state = {
  books: [],
  questions: [],

  isFetchingBooks: false,
  isFetchingBooksError: null,

  isFetchingQuestions: false,
  isFetchingQuestionsError: null,

  isStoringAnswer: false,
  isStoringAnswerError: null
}

const mutations = {
  setFetchingBooks (state) {
    state.isFetchingBooks = true
    state.isFetchingBooksError = null
  },

  setFetchingBooksSuccess (state, data) {
    state.books = data
    state.isFetchingBooks = false
  },

  setFetchingBooksError (state, error) {
    state.isFetchingBooksError = error
    state.isFetchingBooks = false
  },

  setFetchingQuestions (state) {
    state.isFetchingQuestions = true
    state.isFetchingQuestionsError = null
  },

  setFetchingQuestionsSuccess (state, data) {
    state.questions = data
    state.isFetchingQuestions = false
  },

  setFetchingQuestionsError (state, error) {
    state.isFetchingQuestionsError = error
    state.isFetchingQuestions = false
  },

  setStoringAnswer (state) {
    state.isStoringAnswer = true
    state.isStoringAnswerError = null
  },

  setStoringAnswerSuccess (state) {
    state.isStoringAnswer = false
  },

  setStoringAnswerError (state, error) {
    state.isStoringAnswerError = error
    state.isStoringAnswer = false
  }
}

const actions = {
  async getBooks ({ commit }) {
    try {
      commit('setFetchingBooks')
      const res = await author.getBooks()
      commit('setFetchingBooksSuccess', res)
    } catch (error) {
      commit('setFetchingBooksError', error)
    }
  },

  async getQuestions ({ commit }) {
    try {
      commit('setFetchingQuestions')
      const res = await author.getQuestions()
      commit('setFetchingQuestionsSuccess', res)
    } catch (error) {
      commit('setFetchingQuestionsError', error)
    }
  },

  async storeAnswer ({ commit }, payload) {
    try {
      commit('setStoringAnswer')
      await author.storeAnswer(payload)
      commit('setStoringAnswerSuccess')
    } catch (error) {
      commit('setStoringAnswerError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
