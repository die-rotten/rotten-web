import auth from './auth'
import author from './author'
import book from './book'
import bookPackage from './package'
import menu from './menu'
import myBook from './myBook'
import myPackage from './myPackage'
import myTransaction from './myTransaction'

export default {
  auth,
  author,
  book,
  bookPackage,
  menu,
  myBook,
  myPackage,
  myTransaction
}
