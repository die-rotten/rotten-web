import myBook from '@/services/myBook'

const state = {
  book: {},
  content: {},
  books: [],
  tickets: [],

  isFetching: false,
  isFetchingError: null,

  isFinding: false,
  isFindingError: null,

  isFetchingTicket: false,
  isFetchingTicketError: null,

  isStoringQuestion: false,
  isStoringQuestionError: null
}

const mutations = {
  setFetching (state) {
    state.isFetching = true
    state.isFetchingError = null
  },

  setFetchingSuccess (state, data) {
    state.books = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setFinding (state) {
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    const parsedContent = JSON.parse(data.content.content).content

    state.content = {
      ...data.content,
      content: parsedContent
    }
    state.book = data.book
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  },

  setFetchingTicket (state) {
    state.isFetchingTicket = true
    state.isFetchingTicketError = null
  },

  setFetchingTicketSuccess (state, tickets) {
    state.tickets = tickets
    state.isFetchingTicket = false
  },

  setFetchingTicketError (state, error) {
    state.isFetchingTicketError = error
    state.isFetchingTicket = false
  },

  setStoringQuestion (state) {
    state.isStoringQuestion = true
    state.isStoringQuestionError = null
  },

  setStoringQuestionSuccess (state) {
    state.isStoringQuestion = false
  },

  setStoringQuestionError (state, error) {
    state.isStoringQuestion = false
    state.isStoringQuestionError = error
  }
}

const actions = {
  async read ({ commit }, params) {
    try {
      commit('setFinding')
      const contentRes = await myBook.read(params.bookId, params.contentId)
      const bookRes = await myBook.find(params.bookId)

      const data = {
        book: bookRes,
        content: contentRes
      }
      commit('setFindingSuccess', data)
    } catch (error) {
      commit('setFindingError', error)
    }
  },

  async get ({ commit }) {
    try {
      commit('setFetching')
      const res = await myBook.get()
      commit('setFetchingSuccess', res)
    } catch (error) {
      commit('setFetchingError', error)
    }
  },

  async getTicket ({ commit }, contentId) {
    try {
      commit('setFetchingTicket')
      const res = await myBook.getTicket(contentId)
      commit('setFetchingTicketSuccess', res)
    } catch (error) {
      commit('setFetchingTicketError', error)
    }
  },

  async storeQuestion ({ commit }, payload) {
    try {
      commit('setStoringQuestion')
      await myBook.storeQuestion(payload)
      commit('setStoringQuestionSuccess')
    } catch (error) {
      commit('setStoringQuestionError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
