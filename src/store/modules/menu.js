const state = {
  isPurchaseDrawerOpen: false
}

const mutations = {
  setPurchaseDrawer (state, value) {
    state.isPurchaseDrawerOpen = value
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
