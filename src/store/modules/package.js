import bookPackage from '@/services/bookPackage'

const state = {
  packages: [],
  item: {
    books: []
  },

  isFetching: false,
  isFetchingError: null,

  isFinding: false,
  isFindingError: null,

  isPackagePurchased: false
}

const mutations = {
  setFetching (state) {
    state.isFetching = true
    state.isFetchingError = null
  },

  setFetchingSuccess (state, data) {
    state.packages = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setFinding (state) {
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    state.item = {
      ...data,
      books: data.books.data
    }
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  },

  setPackagePurchased (state, value) {
    state.isPackagePurchased = value
  }
}

const actions = {
  async get ({ commit }) {
    try {
      commit('setFetching')
      const res = await bookPackage.get()
      commit('setFetchingSuccess', res)
    } catch (error) {
      commit('setFetchingError', error)
    }
  },

  async find ({ commit }, id) {
    try {
      commit('setFinding')
      const res = await bookPackage.find(id)
      commit('setFindingSuccess', res)
    } catch (error) {
      commit('setFindingError', error)
    }
  },

  async validatePackage ({ commit }, id) {
    try {
      const res = await bookPackage.validate(id)
      commit('setPackagePurchased', res)
    } catch (error) {
      commit('setFindingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
