import auth from '@/services/auth'
import { remove } from '@/plugins/cookie'
import { ACCESS_TOKEN_KEY } from '@/shared'

const state = {
  isAuthenticated: false,
  isAuthenticatingError: null,
  isAuthenticating: false,

  isRegistering: false,
  isRegisteringError: null,
  isRegisteringSuccess: false,

  isChangingPassword: false,
  isChangingPasswordError: null,
  isChangingPasswordSuccess: false,

  isUpdating: false,
  isUpdatingError: null,

  form: {},

  loggedUser: {
    id: 0,
    email: '',
    name: '',
    address: '',
    phoneNumber: '',
    roleId: 0
  }
}

const getters = {
  loggedUser: state => state.loggedUser
}

const mutations = {
  setLoggedUser (state, user) {
    state.loggedUser = {
      id: user.id,
      email: user.email,
      roleId: user.role.data.id,
      name: user.personal.data.fullName,
      address: user.personal.data.address,
      phoneNumber: user.personal.data.phoneNumber
    }

    state.isAuthenticated = true
  },

  logout (state) {
    state.loggedUser = {
      id: 0,
      email: '',
      name: '',
      address: '',
      phoneNumber: '',
      roleId: 0
    }

    state.isAuthenticated = false
    remove(ACCESS_TOKEN_KEY)
  },

  setAuthLoading (state) {
    state.isAuthenticating = true
    state.isAuthenticatingError = null
  },

  setAuthError (state, error) {
    state.isAuthenticatingError = error
    state.isAuthenticating = false
  },

  setAuthSuccess (state) {
    state.isAuthenticating = false
    state.isAuthenticatingError = null
  },

  setRegistering (state) {
    state.isRegistering = true
    state.isRegisteringSuccess = false
    state.isRegisteringError = null
  },

  setRegisteringError (state, error) {
    state.isRegisteringError = error
    state.isRegistering = false
  },

  setRegisteringSuccess (state) {
    state.isRegisteringSuccess = true
    state.isRegistering = false
    state.isRegisteringError = null
  },

  setChangingPassword (state) {
    state.isChangingPassword = true
    state.isChangingPasswordError = null
    state.isChangingPasswordSuccess = false
  },

  setChangingPasswordSuccess (state) {
    state.isChangingPasswordSuccess = true
    state.isChangingPassword = false
  },

  setChangingPasswordError (state, error) {
    state.isChangingPasswordError = error
    state.isChangingPassword = false
  },

  resetChangingPassword (state) {
    state.isChangingPasswordError = null
    state.isChangingPasswordSuccess = false
  },

  setEditing (state) {
    state.form = {
      ...state.loggedUser
    }
  },

  setUpdating (state) {
    state.isUpdating = true
    state.isUpdatingError = null
  },

  setUpdatingSuccess (state) {
    state.isUpdating = false
  },

  setUpdatingError (state, error) {
    state.isUpdatingError = error
    state.isUpdating = false
  }
}

const actions = {
  async auth (context, payload) {
    try {
      context.commit('setAuthLoading')
      await auth.authenticate(payload)
      context.commit('setAuthSuccess')

      const res = await auth.whoami()
      context.commit('setLoggedUser', res)
    } catch (error) {
      context.commit('setAuthError', error)
    }
  },

  async register (context, payload) {
    try {
      context.commit('setRegistering')
      await auth.register(payload)
      context.commit('setRegisteringSuccess')
    } catch (error) {
      context.commit('setRegisteringError', error)
    }
  },

  async whoami (context) {
    try {
      const res = await auth.whoami()

      context.commit('setLoggedUser', res)
    } catch (error) {
      return error
    }
  },

  async changePassword ({ commit }, payload) {
    try {
      commit('setChangingPassword')
      await auth.changePassword(payload)
      commit('setChangingPasswordSuccess')
    } catch (error) {
      commit('setChangingPasswordError', error)
    }
  },

  async update ({ commit }, payload) {
    try {
      commit('setUpdating')
      await auth.update(payload.id, payload)
      commit('setUpdatingSuccess')
    } catch (error) {
      commit('setUpdatingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
