import store from '@/store'
import { get } from '@/plugins/cookie'
import { ACCESS_TOKEN_KEY } from '@/shared'

export async function auth (to, from, next) {
  const token = get(ACCESS_TOKEN_KEY)

  if (!token) {
    if (to.meta.requiresAuth) {
      next({ name: 'login' })
    }

    next()
  } else {
    var user = store.getters['auth/loggedUser']
    if (user.roleId === 0) { await store.dispatch('auth/whoami') }

    user = store.getters['auth/loggedUser']

    if (to.meta.roleIds.includes(user.roleId)) {
      next()
    } else {
      next({ name: 'login' })
    }
  }
}
