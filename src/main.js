import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import http from './services/http'

// import plugins
import './plugins/element-ui'
import './plugins/vue-mq'
import './plugins/vuelidate'

// import global component
import AppFormItem from './components/layout/AppFormItem'

import './assets/styles/global.scss'

http.init()

Vue.config.productionTip = false

Vue.component('AppFormItem', AppFormItem)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
