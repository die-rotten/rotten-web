export function textLimit (value, length = 100) {
  if (value.length >= length) {
    return value.slice(0, length - 1) + '...'
  }

  return value
}
