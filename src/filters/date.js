import dayjs from 'dayjs'
import id from 'dayjs/locale/id'

export function date (value) {
  return dayjs(value).format('DD/MM/YYYY')
}

export function extendedDate (value) {
  return dayjs(value).locale(id).format('DD MMMM YYYY HH:mm:ss')
}
