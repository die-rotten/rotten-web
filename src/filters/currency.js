import currency from 'currency.js'

export const RUPIAH = value => currency(value, { symbol: 'Rp ' })

export function rupiah (value) {
  return RUPIAH(value).format(true)
}
