import http from './http'
import { get, set } from '@/plugins/cookie'
import { ACCESS_TOKEN_KEY } from '@/shared'

export default {
  async authenticate ({ email, password }) {
    try {
      const response = await http.post('/v1/auth', {
        username: email,
        password: password,
        grant_type: 'password',
        client_secret: process.env.VUE_APP_CLIENT_SECRET,
        client_id: process.env.VUE_APP_CLIENT_ID
      })

      http.setHeader(response.data.access_token)
      set(ACCESS_TOKEN_KEY, response.data.access_token)

      return response.data.access_token
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async register (payload) {
    try {
      const response = await http.post('/v1/us/register', {
        email: payload.email,
        password: payload.password,
        confirmationPassword: payload.confirmationPassword,
        fullName: payload.fullName,
        phoneNumber: payload.phoneNumber,
        isAuthor: false
      })

      return response.data.data
    } catch (error) {
      if (error.response.data.email[0] === 'The email has already been taken.') {
        throw new Error('Already taken')
      }

      throw new Error(error.message)
    }
  },

  async whoami () {
    try {
      const token = get(ACCESS_TOKEN_KEY)
      http.setHeader(token)
      const response = await http.get('/v1/whoami')

      return response.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async changePassword (payload) {
    try {
      const response = await http.post('/v1/us/users/change-password', {
        oldPassword: payload.currentPassword,
        newPassword: payload.newPassword,
        newPasswordConfirmation: payload.confirmationPassword
      })

      return response.data.data
    } catch (error) {
      throw new Error(error.response.data)
    }
  },

  async update (id, payload) {
    try {
      const response = await http.patch(`/v1/us/users/${id}`, {
        fullName: payload.name,
        phoneNumber: payload.phoneNumber,
        address: payload.address
      })

      return response.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
