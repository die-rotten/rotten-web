import http from './http'

export default {
  async getMyPackage () {
    try {
      const res = await http.get('/v1/cs/my/packages')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async findMyPackage (id) {
    try {
      const res = await http.get(`/v1/cs/my/packages/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
