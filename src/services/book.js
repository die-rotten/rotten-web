import http from './http'

export default {
  async find (id) {
    try {
      const res = await http.get(`/v1/cs/books/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
