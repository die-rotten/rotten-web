import http from './http'

export default {
  async read (bookId, contentId) {
    try {
      const res = await http.get(`/v1/cs/my/books/${bookId}/read/${contentId}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async find (bookId) {
    try {
      const res = await http.get(`/v1/cs/my/books/${bookId}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async get () {
    try {
      const res = await http.get('/v1/cs/my/books/latest?limit=3')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async getTicket (contentId) {
    try {
      const res = await http.get(`/v1/cs/questions/${contentId}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async storeQuestion (payload) {
    try {
      const res = await http.post('/v1/cs/questions', payload)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
