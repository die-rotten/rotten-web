import http from './http'

export default {
  async get (params) {
    try {
      const res = await http.get('/v1/ts/transactions/by-user', params)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async find (id) {
    try {
      const res = await http.get(`/v1/ts/transactions/by-user/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async store (payload) {
    try {
      const res = await http.post('/v1/ts/transactions', payload)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async confirmPayment (id, payload) {
    try {
      const res = await http.post(`/v1/ts/confirmations/${id}`, payload)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
