import http from './http'

export default {
  async get () {
    try {
      const res = await http.get('/v1/cs/packages')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async getMyPackage () {
    try {
      const res = await http.get('/v1/cs/my/packages')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async find (id) {
    try {
      const res = await http.get(`/v1/cs/packages/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async validate (id) {
    try {
      const res = await http.get(`/v1/cs/packages/${id}/validate`)

      return res.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
