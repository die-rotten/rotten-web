import http from './http'

export default {
  async getBooks () {
    try {
      const res = await http.get('/v1/cs/books/by-author')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async getQuestions () {
    try {
      const res = await http.get('/v1/cs/questions/for-author')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async storeAnswer (payload) {
    try {
      const res = await http.post('/v1/cs/answers', payload)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
