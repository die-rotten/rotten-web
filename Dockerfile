# First stage
FROM node:alpine AS build

WORKDIR /build

ADD . .

RUN yarn install
RUN yarn build

# Second stage
FROM nginx:stable-alpine

WORKDIR /app

ADD ./docker/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /build/dist /var/www/html/
